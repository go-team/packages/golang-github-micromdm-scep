Source: golang-github-micromdm-scep
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Peymaneh <peymaneh@posteo.net>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-boltdb-bolt-dev,
               golang-github-fullsailor-pkcs7-dev (>= 0.0~git20210826.33d0574~),
               golang-github-go-kit-kit-dev,
               golang-github-gorilla-mux-dev,
               golang-github-pkg-errors-dev,
Standards-Version: 4.6.1.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-micromdm-scep
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-micromdm-scep.git
Homepage: https://github.com/micromdm/scep
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/micromdm/scep

Package: scep
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: SCEP server and client written in go
 scep implements Simple Certificate Enrollment Protocol server and client.
 This package provides its binaries scepserver and scepclient.

Package: golang-github-micromdm-scep-dev
Architecture: all
Depends: golang-github-boltdb-bolt-dev,
         golang-github-fullsailor-pkcs7-dev (>= 0.0~git20210826.33d0574~),
         golang-github-go-kit-kit-dev,
         golang-github-gorilla-mux-dev,
         golang-github-pkg-errors-dev,
         ${misc:Depends},
Description: SCEP go library
 This package provides a Simple Certificate Enrollment Protocol library for
 usage in another go project.
 .
 An example server implementation will be installed to
 /usr/share/doc/golang-github-micromdm-scep-dev/examples/
 .
 Documentation: https://pkg.go.dev/github.com/micromdm/scep/v2/scep
